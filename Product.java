public class Product {
    private String productName;
    private int cost;
    private float rating;

    Product(String name, int cost, float rating){
        this.cost=cost;
        this.productName=name;
        this.rating=rating;
    }

    public int getCost() {
        return this.cost;
    }

    public String getProductName(){
        return this.productName;
    }

    public void showProductInfo(){
        System.out.println(cost + " " + productName + " " + rating);
    }
}
