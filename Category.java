import java.util.ArrayList;

public class Category {
    private ArrayList<Product> products;
    private String categoryName;

    Category(ArrayList<Product> products, String categoryName) {
        this.products.addAll(products);
        this.categoryName = categoryName;
    }

    Category(String categoryName){
        this.categoryName=categoryName;
        this.products= new ArrayList<>();
    }

    public void addProduct(Product product){
        this.products.add(product);
    }
}
