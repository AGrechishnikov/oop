public class Main {
    public static void main(String[] args) {
        Category computerCategory = new Category("Computer details");

        Product gpu = new Product("rtx 3090",1000, 4.56f);
        Product cpu = new Product("intel i7",500, 4.69f);

        computerCategory.addProduct(gpu);
        computerCategory.addProduct(cpu);

        Product boxingGloves = new Product("Boxing Gloves", 10, 4.87f);
        Product ball = new Product("Ball",20,4.00f);

        Category sportsCategory = new Category("Sport");

        sportsCategory.addProduct(boxingGloves);
        sportsCategory.addProduct(ball);

        User firstUser = new User("login","password");

        firstUser.addToBasket(gpu);
        firstUser.addToBasket(boxingGloves);

        firstUser.showInfo();
    }
}
