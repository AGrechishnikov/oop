import java.util.ArrayList;

public class Basket {
    private ArrayList<Product> products;

    Basket(){
        products=new ArrayList<>();
    }
    public void add(Product productToAdd){
        products.add(productToAdd);
    }

    public void showBasket(){
        for(Product product: products){
            product.showProductInfo();
        }

    }
}
