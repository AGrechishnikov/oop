public class User {
    private String login;
    private String password;
    private Basket basket;

    User(String login, String password){
        this.login=login;
        this.password=password;
        this.basket=new Basket();
    }

    public void addToBasket(Product productToAdd){
        basket.add(productToAdd);
    }

    public void showInfo(){
        System.out.println("User's login and password:" + this.login + " " + this.password);
        System.out.println("User's products in basket:");
        this.basket.showBasket();
    }
}
